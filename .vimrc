execute pathogen#infect()
syn on
set number
filetype plugin on
au FileType python set omnifunc=pythoncomplete#Complete
set modeline
set tabstop=8 expandtab shiftwidth=4 softtabstop=4
au FileType javascript call JavaScriptFold()
au BufNewFile,BufRead *.ejs set filetype=html
